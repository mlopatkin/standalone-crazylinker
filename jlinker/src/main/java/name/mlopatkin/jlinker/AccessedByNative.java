package name.mlopatkin.jlinker;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

// Class Retention because it must be visible to Proguard
@Retention(RetentionPolicy.CLASS)
public @interface AccessedByNative {
}
