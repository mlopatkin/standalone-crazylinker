package name.mlopatkin.jlinker;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;

public class JLinker {

    static {
        System.loadLibrary("jlinker");

        nativeInit(Build.VERSION.SDK_INT);
    }

    private static final Handler sHandler = new Handler(Looper.getMainLooper());

    public static void loadLibaryFromZipFile(String apkPath, String libPath) {
        if (!nativeLoadLibraryInZipFile(apkPath, libPath)) {
            throw new UnsatisfiedLinkError("Failed to load " + libPath + " from APK");
        }
    }

    public static void loadLibrary(String libraryPath) {
        if (!nativeLoadLibrary(libraryPath)) {
            throw new UnsatisfiedLinkError("Failed to load " + libraryPath);
        }
    }

    @AccessedByNative
    private static void postCallbackOnMainThread(final long opaque) {
        sHandler.post(new Runnable() {
            @Override
            public void run() {
                nativeRunCallbackOnUiThread(opaque);
            }
        });
    }

    private static native boolean nativeLoadLibraryInZipFile(String zipFilePath, String libPath);
    private static native boolean nativeLoadLibrary(String libPath);
    private static native void nativeInit(int sdkVersion);
    private static native void nativeRunCallbackOnUiThread(long opaque);
}
