#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR/jlinker/src/main
ndk-build

cd $DIR/tools/relocation_packer/elfutils
make libelf.a

cd $DIR/tools/relocation_packer/android_platform
make android_relocation_packer

cd $DIR/sample/src/main
ndk-build

$DIR/tools/pack_relocations.py \
 --enable-packing=1 \
 --android-pack-relocation=$DIR/tools/relocation_packer/android_platform/android_relocation_packer \
 --stripped-libraries-dir=$DIR/sample/src/main/libs/armeabi-v7a \
 --packed-libraries-dir=$DIR/sample/src/main/libs-packed/armeabi-v7a \
 --libraries=libhello-jni.so \
 --clear-dir

cd $DIR
./gradlew assembleDebug

