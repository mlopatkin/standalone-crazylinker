package name.mlopatkin.hellocrazylinker;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import name.mlopatkin.jlinker.JLinker;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        JLinker.loadLibrary(getApplicationInfo().nativeLibraryDir + "/libhello.so");

        TextView helloView = (TextView) findViewById(R.id.hello_view);

        helloView.setText(HelloJni.stringFromJni(1));
    }
}
