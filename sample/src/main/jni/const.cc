#include "const.h"

// We need a lot of constants for relocation_packer to gain something
#define CONSTANT(x) const char x[] = #x;
#include "consts.inc"
#undef CONSTANT
