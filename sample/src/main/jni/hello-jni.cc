#include <jni.h>

#include "const.h"

extern const char *kArchitectures[];

const char *kArchitectures[] = {
#define CONSTANT(x) x,

#include "consts.inc"
#undef CONSTANT
};

namespace {

jstring HelloJni_stringFromJNI(JNIEnv *env, jobject thiz, jint index) {
  return env->NewStringUTF(kArchitectures[index]);
}

JNINativeMethod kNativeMethods[] = {
    {"stringFromJni",
     "(I)Ljava/lang/String;",
     reinterpret_cast<void*>(&HelloJni_stringFromJNI)
    },
};

}  // namespace

extern "C" JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved) {
  JNIEnv *env;

  if (vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_4) != JNI_OK) {
    return -1;
  }

  jclass hello_class = env->FindClass("name/mlopatkin/hellocrazylinker/HelloJni");
  if (!hello_class) {
    return -1;
  }

  env->RegisterNatives(hello_class,
                       kNativeMethods,
                       sizeof(kNativeMethods) / sizeof(kNativeMethods[0]));
  return JNI_VERSION_1_4;
}

