package name.mlopatkin.jlinker.build;

import com.android.build.gradle.tasks.StripDebugSymbolTask;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;


public class PackRelocations extends DefaultTask {

    private File executable;
    private File inputFolder;

    @TaskAction
    void packRelocations() throws IOException, InterruptedException {
        FileFilter libFilesFilter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                String name = pathname.getName();
                return pathname.isFile() && name.startsWith("lib") && name.endsWith(".so")
                        && !name.equals("libjlinker.so");
            }
        };

        for (File file : inputFolder.listFiles(libFilesFilter)) {
            ProcessBuilder b = new ProcessBuilder(executable.getAbsolutePath(),
                    file.getAbsolutePath());
            b.redirectOutput(ProcessBuilder.Redirect.INHERIT);
            b.redirectErrorStream(true);

            b.start().waitFor();
        }
    }

    public static PackRelocations createForTask(StripDebugSymbolTask task,
                                                File relocationPackerExecutable) {
        PackRelocations packTask = task.getProject().getTasks()
                .create("packRelocationsFor" + task.getName(), PackRelocations.class);

        packTask.executable = relocationPackerExecutable;
        packTask.inputFolder = task.getOutputFolder();
        return packTask;
    }

}